﻿using Namarina2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Namarina2.ViewModels
{
    public class PaymentDetails
    {
        public int StudentId { get; set; }
        public string StudentCode { get; set; }
        public string StudentName { get; set; }
        public List<MonthlyPayment> MonthlyPayments { get; set; }
        public string MonthlyPaymentsString { get; set; }
        public decimal GrandTotalPrice { get; set; }
        public decimal? ChangedPrice { get; set; }
        public string ChangePriceReason { get; set; }
        public decimal TotalPenalty { get; set; }
        public decimal TotalPrice { get; set; }
        public bool PayPenalty { get; set; }
        public string Status { get; set; }
        public decimal? LastPrice { get; set; }
        public string LastClasses { get; set; }
        public DateTime? LastPeriod { get; set; }
        public DateTime? PaymentDate { get; set; }
    }

    public class MonthlyPayment
    {
        public DateTime PaymentPeriod { get; set; }
        public string Classes { get; set; }
        public decimal Price { get; set; }
        public decimal Penalty { get; set; }
    }
}