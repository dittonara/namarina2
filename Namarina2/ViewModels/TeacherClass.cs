﻿using Namarina2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Namarina2.ViewModels
{
    public class TeacherClass
    {
        public List<int> AssignedClassIds { get; set; }
        public int TeacherId { get; set; }
        public string TeacherName {get;set;}
        public ICollection<string> AssignedClassNames {get;set;}
        public List<Namarina2.Models.Class> BalletClasses { get; set; }
        public List<Namarina2.Models.Class> JazzClasses { get; set; }
        public List<Namarina2.Models.Class> FitnessClasses { get; set; }
    }
}