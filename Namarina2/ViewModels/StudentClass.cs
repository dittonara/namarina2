﻿using Namarina2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Namarina2.ViewModels
{
    public class StudentClass
    {
        public List<int> TakenClassIds { get; set; }
        public int StudentId { get; set; }
        public string StudentName {get;set;}
        public ICollection<string> TakenClassNames {get;set;}
        public ICollection<Enrollment> Enrollments { get; set; }
        public int? TakenPackageId { get; set; }
        public List<Namarina2.Models.Class> BalletClasses { get; set; }
        public List<Namarina2.Models.Class> JazzClasses { get; set; }
        public List<Namarina2.Models.Class> FitnessClasses { get; set; }
        public List<Namarina2.Models.ClassPackage> ClassPackages { get; set; }
    }
}