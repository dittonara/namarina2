﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Namarina2.ViewModels
{
    public class StudentPayment
    {
        public int StudentId { get; set; }
        public string StudentCode { get; set; }
        public string StudentName { get; set; }
        public string PaymentPeriods { get; set; }
    }
}