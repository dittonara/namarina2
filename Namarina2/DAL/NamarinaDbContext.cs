﻿using Namarina2.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Namarina2.DAL
{
    public class NamarinaDbContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public DbSet<Class> Classes { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }
        public DbSet<Guardian> Guardians { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<InvoicesPusat> InvoicesPusat { get; set; }
        public DbSet<InvoicesKebayoran> InvoicesKebayoran { get; set; }
        public DbSet<RegistrationInvoice> RegistrationInvoices { get; set; }        
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<TeacherAssignment> TeacherAssignments { get; set; }
        public DbSet<ClassPackage> ClassPackages { get; set; }
        public DbSet<NDAPrice> NDAPrices { get; set; }
        public DbSet<TeachDay> TeachDays { get; set; }
    }
}