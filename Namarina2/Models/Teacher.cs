﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Namarina2.Models
{
    public class Teacher
    {
        public int TeacherID { get; set; }        
        [Required]
        [DisplayName("Nama")]
        [MaxLength(250)]
        public string Name { get; set; }
        [MaxLength(250)]
        public string BirthPlace { get; set; }
        public int Tanggal { get; set; }
        public int Bulan { get; set; }
        public int Tahun {get;set;}
         [DisplayName("Tgl Lahir")]
        public DateTime? BirthDate
        {
            get
            {
                return new DateTime(Tahun, Bulan, Tanggal);
            }
        }
        public virtual ICollection<TeacherAssignment> TeacherAssignments { get; set; }
        [MaxLength(10)]
        [Required]
        public string Gender { get; set; }
        public string Religion { get; set; }
        [DisplayName("Alamat")]
        [MaxLength(250)]
        public string Address { get; set; }
        [DisplayName("Telepon Rumah")]
        [MaxLength(50)]
        public string LandlinePhoneNo { get; set; }
        [DisplayName("No HP")]
        [MaxLength(50)]
        public string MobilePhoneNo { get; set; }
        public string Email { get; set; }
        public decimal HourlyPay { get; set; }
        public decimal DailyTransport { get; set; }
        public string Division { get; set; }
        public string Branch { get; set; }
        public DateTime? JoinedDate { get; set; }
    }
}