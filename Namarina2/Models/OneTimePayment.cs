﻿using Foolproof;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Namarina2.Models
{
    public class OneTimePayment
    {        
        [DisplayName("Nama")]
        [MaxLength(250)]
        [Required]
        public string Name { get; set; }

        [Required]
        public int ClassId { get; set; }     
   
        public int? FixedPrice { get; set; }
        [RequiredIfNotEmpty("FixedPrice")]
        public string ReasonFixedPrice { get; set; }

        public static List<SelectListItem> GetClassList()
        {
            var db = new Namarina2.DAL.NamarinaDbContext();
            var classes = db.Classes;
            var result = new List<SelectListItem>();
            foreach (var c in classes)
            {
                result.Add(new SelectListItem { Value = c.ClassID.ToString(), Text = c.ClassCode + " - " + c.Name });
            }
            return result;
        }
    }   
}