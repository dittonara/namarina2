﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Namarina2.Models
{
    public class Guardian
    {
        public int GuardianID { get; set; }

        [MaxLength(100)]
        [Required]
        [DisplayName("Nama")]
        public string Name { get; set; }

        [MaxLength(20)]
        [Required]
        [DisplayName("Hubungan")]
        public string Relationship { get; set; }

        [MaxLength(50)]
        public string Email { get; set; }

        [MaxLength(50)]
        [DisplayName("No Telepon Rumah")]
        public string LandlinePhoneNumber { get; set; }

        [MaxLength(50)]
        [Required]
        [DisplayName("No HP")]
        public string MobilePhoneNumber { get; set; }

        [MaxLength(250)]
        [Required]
        [DisplayName("Alamat Rumah")]
        public string Address { get; set; }

        [MaxLength(50)]
        [DisplayName("Pekerjaan")]
        public string Job { get; set; }

        [MaxLength(200)]
        [DisplayName("Nama Kantor")]
        public string OfficeName { get; set; }

        [MaxLength(250)]
        [DisplayName("Alamat Kantor")]
        public string OfficeAddress { get; set; }

        [MaxLength(50)]
        [DisplayName("No Telp Kantor")]
        public string OfficePhone { get; set; }

        public int StudentID { get; set; }
        public virtual Student Student { get; set; }

        public bool IsMainGuardian { get; set; }
    }
}