﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Namarina2.Models
{
    public class ClassTimeTable
    {

        public int ClassTimeTableID { get; set; }
        public int ClassID { get; set; }
        public string Day { get; set; }
        public string StartHour { get; set; }
        public string EndHour { get; set; }      
    }
}