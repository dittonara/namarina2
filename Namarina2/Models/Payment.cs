﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Namarina2.Models
{
    public class Payment
    {
        [Key]
        public int PaymentID { get; set; }
        public DateTime? PaymentDate { get; set; }
        public DateTime PaymentPeriod { get; set; }
        public virtual int StudentID { get; set; }
        public virtual Student Student { get; set; }
        [MaxLength(10)]
        public string StudentCode { get; set; }
        [MaxLength(250)]
        public string StudentName { get; set; }
        [MaxLength(500)]
        public string Enrollments { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Penalty { get; set; }
        public decimal? Price { get; set; }
        public decimal? FixedPrice { get; set; }
        public string ReasonFixedPrice { get; set; }
        public bool IsNotPayPenalty { get; set; }
        public bool Paid { get; set; }
        public int? InvoiceID { get; set; }
    }

    public class InvoicesPusat
    {
        [Key]
        public int InvoiceID { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string PaymentPeriodString { get; set; }
        public int StudentID { get; set; }
        [MaxLength(10)]
        public string StudentCode { get; set; }
        [MaxLength(250)]
        public string StudentName { get; set; }
        [MaxLength(500)]
        public string Enrollments { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Penalty { get; set; }
        public decimal? Price { get; set; }
        public decimal? FixedPrice { get; set; }
        public string ReasonFixedPrice { get; set; }
        public bool IsNotPayPenalty { get; set; }
        public bool Paid { get; set; }
        [MaxLength(250)]
        public string AdminUserName { get; set; }
        [MaxLength(50)]
        public string Branch { get; set; }
    }
    public class InvoicesKebayoran
    {
        [Key]
        public int InvoiceID { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string PaymentPeriodString { get; set; }
        public int StudentID { get; set; }
        [MaxLength(10)]
        public string StudentCode { get; set; }
        [MaxLength(250)]
        public string StudentName { get; set; }
        [MaxLength(500)]
        public string Enrollments { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Penalty { get; set; }
        public decimal? Price { get; set; }
        public decimal? FixedPrice { get; set; }
        public string ReasonFixedPrice { get; set; }
        public bool IsNotPayPenalty { get; set; }
        public bool Paid { get; set; }
        [MaxLength(250)]
        public string AdminUserName { get; set; }
        [MaxLength(50)]
        public string Branch { get; set; }
    }

    public class RegistrationInvoice
    {
        public int RegistrationInvoiceID { get; set; }
        public DateTime? PaymentDate { get; set; }
        public int StudentID { get; set; }
        [MaxLength(10)]
        public string StudentCode { get; set; }
        [MaxLength(250)]
        public string StudentName { get; set; }
        [MaxLength(500)]
        public string Enrollments { get; set; }
        public decimal? Price { get; set; }
        public decimal? FixedPrice { get; set; }
        public string ReasonFixedPrice { get; set; }
        public int Status { get; set; }
        [MaxLength(250)]
        public string AdminUserName { get; set; }
        [MaxLength(50)]
        public string Branch { get; set; }
        public DateTime? PaymentPeriod { get; set; }
    }

}