﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Namarina2.Models
{
    public class TeacherAssignment
    {
        public int TeacherAssignmentID { get; set; }
        public int? ClassID { get; set; }
        public int? TeacherID { get; set; }
        public DateTime? AssignedDate { get; set; }
        public virtual Class Class { get; set; }
        public virtual Teacher Teacher { get; set; }
        //public virtual ICollection<Enrollment> Enrollments { get; set; }
    }
}