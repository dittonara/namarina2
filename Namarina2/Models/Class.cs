﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Namarina2.Models
{
    public class Class
    {
        public int ClassID { get; set; }
        [MaxLength(50)]
        [Required]
        public string ClassCode { get; set; }

        [MaxLength(100)]
        [Required]
        public string Name {get;set;}

        [MaxLength(50)]
        [Required]
        public string Type { get; set; }

        [MaxLength(50)]
        public string SubType { get; set; }

        public decimal Price { get; set; }
        public decimal? OneTimePrice { get; set; }
        public decimal? OneTimePriceMember { get; set; }

        public string Branch { get; set; }

        public virtual ICollection<Enrollment> Enrollments { get; set; }

        public virtual ICollection<TeacherAssignment> TeacherAssignments { get; set; }

        public string Day { get; set; }
        public DateTime? StartHour { get; set; }
        public DateTime? EndHour { get; set; }

        public int? ClassPackageID { get; set; }
     }
}