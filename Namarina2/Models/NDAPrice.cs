﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Namarina2.Models
{
    public class NDAPrice
    {
        public int NDAPriceId { get; set; }
        [MaxLength(50)]
        [Required]
        public string ClassCode { get; set; }

        [MaxLength(100)]
        [Required]
        public string Name {get;set;}
       
        public decimal Price { get; set; }
        public decimal? DiscPrice10 { get; set; }
        public decimal? DiscPrice50 { get; set; }

     }
}