﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Namarina2.Models
{
    public class TeachDay
    {
        public int TeachDayID { get; set; }
        public int TeacherID { get; set; }
        public string Day { get; set; }
        public DateTime StartHour { get; set; }
        public DateTime EndHour { get; set; }
    }
}