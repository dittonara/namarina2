﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Namarina2.Models
{
    public class Student
    {
        public int StudentID { get; set; }

        [DisplayName("Kode")]
        [MaxLength(10)]
        public string StudentCode { get; set; }

        [DisplayName("Nama")]
        [MaxLength(250)]
        [Required]
        public string Name { get; set; }

        [MaxLength(100)]
        [DisplayName("Panggilan")]
        public string NickName { get; set; }

        [DisplayName("Tgl Masuk")]
        public DateTime? AdmissionDate { get; set; }

        [UIHint("Enrollments")]
        public virtual ICollection<Enrollment> Enrollments { get; set; }

        public virtual ICollection<Guardian> Guardians { get; set; }

        public int? ClassPackageID { get; set; }
       
        [MaxLength(10)]
        [Required]
        public string Gender { get; set; }

        [MaxLength(100)]
        public string PlaceOfBirth { get; set; }

        [DisplayName("Tgl Lahir")]
        public DateTime? BirthDate { get; set; }

        public int Tanggal { get; set; }
        public int Bulan { get; set; }
        public int Tahun { get; set; }

        //public string BloodType { get; set; }
        //public string BirthPlace { get; set; }
        //public string Nationality { get; set; }

        [DisplayName("Agama")]
        [MaxLength(10)]
        public string Religion { get; set; }

        [DisplayName("Alamat")]
        [MaxLength(250)]
        public string Address { get; set; }
        [MaxLength(100)]
        [DisplayName("Kota")]
        public string City { get; set; }
        [MaxLength(10)]
        [DisplayName("Kode Pos")]
        public string PostalCode { get; set; }


        [DisplayName("Telepon Rumah")]
        [MaxLength(50)]
        public string LandLinePhoneNo { get; set; }

        [DisplayName("No HP")]
        [MaxLength(50)]
        public string MobilePhoneNo { get; set; }
        //public string MobilePhoneNo2 { get; set; }

        [MaxLength(50)]
        public string Email { get; set; }

        [UIHint("Photo")]
        [MaxLength(105)]
        public string Photo { get; set; }

        [MaxLength(20)]
        [DisplayName("Cabang")]
        [Required]
        public string Branch { get; set; }

        [MaxLength(10)]
        public string Status { get; set; }

        public bool? IsNDA { get; set; }
        public bool? IsTeacher { get; set; }
        public bool? IsPostPoneOrGraduatedHigherGrade { get; set; }

        [DisplayName("Catatan")]
        public string Remark { get; set; }

        [MaxLength(100)]
        [DisplayName("Nama Sekolah/Universitas")]
        public string SchoolName { get; set; }
        [DisplayName("Alamat Sekolah/Universitas")]
        [MaxLength(250)]
        public string SchoolAddress { get; set; }
        [MaxLength(100)]
        [DisplayName("Kelas/Tingkat di Sekolah/Universitas")]
        public string SchoolClass { get; set; }
        public bool? IsBallet { get; set; }
        public bool? IsJazz { get; set; }
        public bool? IsFitness { get; set; }

        //Questionaire
        public bool? HavePreviousSchool { get; set; }
        [MaxLength(100)]
        public string PreviousSchool { get; set; }
        public string WhereGetInfo { get; set; }
        public string WhyJoin { get; set; }
    }   
}