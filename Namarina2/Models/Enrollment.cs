﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Namarina2.Models
{
    public class Enrollment
    {
        public int EnrollmentID { get; set; }
        public int? ClassID { get; set; }
        public int? StudentID { get; set; }
        public DateTime? EnrollmentDates { get; set; }
        public bool IsPostpone { get; set; }
        public DateTime? PostPoneStartDate { get; set; }
        public virtual Class Class { get; set; }
        public virtual Student Student { get; set; }
    }
}