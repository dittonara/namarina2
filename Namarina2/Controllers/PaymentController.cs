﻿using Kendo.DynamicLinq;
using Namarina2.DAL;
using Namarina2.Filters;
using Namarina2.Models;
using Namarina2.ViewModels;
using Novacode;
using System;
using System.Collections.Generic;
using System.Data;
//using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Spire.Doc;
using Dapper;

namespace Namarina2.Controllers
{
    //[InitializeSimpleMembership]
    public class PaymentController : Controller
    {
        private NamarinaDbContext db = new NamarinaDbContext();

        //
        // GET: /Payment/
        [Authorize]
        public ActionResult Index()
        {
            var model = new PaymentFormModel();
            return View(model);
        }
        //public ActionResult UpdatePaidStatus()
        //{

        //    var db2 = new NamarinaEntities();
        //    var students = (from d in db2.Payments select d.StudentCode).Distinct();
        //    foreach (var s in students)
        //    {
        //        //cari status paid terakhir
        //        var query = (from d in db2.Payments where d.Paid == true && d.StudentCode == s orderby d.PaymentPeriod select d.PaymentPeriod).FirstOrDefault();
        //        if (query != null)
        //        {

        //            //update semua payment period sebelum query
        //            var paymentPeriods = from d in db2.Payments where d.Paid == false && d.StudentCode == s && d.PaymentPeriod < query select d;
        //            foreach (var p in paymentPeriods)
        //            {
        //                p.Paid = true;
        //            }
        //        }
        //    }


        //    db2.SaveChanges();


        //    return View("Success");
        //}

        //public ActionResult UpdatePayments()
        //{

        //    var db2 = new NamarinaEntities();
        //    var query = db2.pembayarans.ToList().Skip(100).Take(100);
        //    foreach (var q in query)
        //    {
        //        var classes = new List<string>();
        //        decimal price = 0;
        //        if (!string.IsNullOrEmpty(q.NDA)) //nda
        //        {
        //            var ndaPriceRow = (from d in db2.NDAPrices where d.Name == q.NDA select d).Single();
        //            if (q.IsTeacher == "yes") //disc 50%
        //            {
        //                price = ndaPriceRow.DiscPrice50.Value;
        //            }
        //            else if (q.IsPassHigherGrade == "yes") //disc 10%
        //            {
        //                price = ndaPriceRow.DiscPrice10.Value;
        //            }
        //            else
        //            {
        //                price = ndaPriceRow.Price;
        //            }
        //            //cek special class
        //            classes = new List<string>();
        //            if (!string.IsNullOrEmpty(q.Kelas1) && q.Kelas1.Contains("SC")) classes.Add(q.Kelas1);
        //            if (!string.IsNullOrEmpty(q.Kelas2) && q.Kelas2.Contains("SC")) classes.Add(q.Kelas2);
        //            if (!string.IsNullOrEmpty(q.Kelas3) && q.Kelas3.Contains("SC")) classes.Add(q.Kelas3);
        //            if (!string.IsNullOrEmpty(q.Kelas4) && q.Kelas4.Contains("SC")) classes.Add(q.Kelas4);
        //            if (!string.IsNullOrEmpty(q.Kelas5) && q.Kelas5.Contains("SC")) classes.Add(q.Kelas5);
        //            decimal totalPrice = 0;
        //            foreach (var c in classes)
        //            {
        //                var classPrice = (from d in db2.Classes where d.ClassCode == c select d.Price).SingleOrDefault();
        //                if (classPrice != null)
        //                {
        //                    totalPrice += classPrice;
        //                }
        //            }
        //            price += totalPrice;

        //            if (q.IsNYD == "yes") //disc 25%
        //            {
        //                price = price - (price * 25 / 100);
        //                price = Math.Ceiling((decimal)price / 5000) * 5000; //buletin 10000 ke atas
        //            }
        //        }
        //        else
        //        { //ketengan
        //            classes = new List<string>();
        //            if (!string.IsNullOrEmpty(q.Kelas1)) classes.Add(q.Kelas1);
        //            if (!string.IsNullOrEmpty(q.Kelas2)) classes.Add(q.Kelas2);
        //            if (!string.IsNullOrEmpty(q.Kelas3)) classes.Add(q.Kelas3);
        //            if (!string.IsNullOrEmpty(q.Kelas4)) classes.Add(q.Kelas4);
        //            if (!string.IsNullOrEmpty(q.Kelas5)) classes.Add(q.Kelas5);
        //            decimal totalPrice = 0;
        //            var isBFAP = false;
        //            var isBFAK = false;
        //            foreach (var c in classes)
        //            {
        //                if (c == "BFA/P") isBFAP = true;
        //                if (c == "BFA/K") isBFAK = true;

        //                var classPrice = (from d in db2.Classes where d.ClassCode == c select d.Price).SingleOrDefault();
        //                if (classPrice != null)
        //                {
        //                    totalPrice += classPrice;
        //                }
        //            }

        //            price = totalPrice;

        //            //cek bfa 2x
        //            if (isBFAP && isBFAK)
        //            {
        //                price = price - 150000;
        //            }
        //        }

        //        //kurangi tebet
        //        var kelasTebet = new List<string>();
        //        classes = new List<string>();
        //        if (!string.IsNullOrEmpty(q.Kelas1)) classes.Add(q.Kelas1);
        //        if (!string.IsNullOrEmpty(q.Kelas2)) classes.Add(q.Kelas2);
        //        if (!string.IsNullOrEmpty(q.Kelas3)) classes.Add(q.Kelas3);
        //        if (!string.IsNullOrEmpty(q.Kelas4)) classes.Add(q.Kelas4);
        //        if (!string.IsNullOrEmpty(q.Kelas5)) classes.Add(q.Kelas5);
        //        foreach (var c in classes)
        //        {
        //            if (c.Contains("/T"))
        //            {
        //                kelasTebet.Add(c);
        //            }
        //        }
        //        decimal totalHargaTebet = 0;
        //        foreach (var k in kelasTebet)
        //        {
        //            var hargaKelasTebet = 200000;// (from d in db2.Classes where d.ClassCode == k select d.Price).Single();
        //            totalHargaTebet += hargaKelasTebet;
        //        }
        //        price = price - totalHargaTebet;
        //        var query2 = (from d in db2.Payments where d.StudentCode == q.Kode select d).ToList();
        //        foreach (var q2 in query2)
        //        {
        //            q2.Price = price;
        //        }

        //        db2.SaveChanges();
        //    }
        //    return View("Success");
        //}
               
        [Authorize]
        public ActionResult GetHaventPaidStudents()
        {

            //var studentPayments = db.Database.SqlQuery<StudentPayment>("select p.StudentId, p.StudentCode, StudentName,substring((select ', ' + right(Convert(varchar,PaymentPeriod,106),8)"
            //                 + "from payments where PaymentPeriod <= getdate() and StudentId = p.StudentId and paid = 0 for xml path ('')), 3,9999) as PaymentPeriods"
            //                 + "from payments p join students s on p.StudentID = s.StudentId"
            //                 + "where s.Status = 1 and p.PaymentPeriod <= getdate()"
            //                 + "group by p.StudentID, p.StudentCode, StudentName OFFSET " + skip + " ROWS FETCH NEXT " + take + "ROWS ONLY", new object[] { }).ToList();

            var studentPayments = db.Database.SqlQuery<StudentPayment>("[dbo].ViewStudentPayment", new object[] { }).ToList();
            var studentData = from m in studentPayments //join s in db.Students on m.StudentId equals s.StudentID
                              //              where s.Status == "1"
                              select new
                {
                    StudentId = m.StudentId,
                    StudentCode = m.StudentCode,
                    Name = m.StudentName,
                    PaymentPeriods = m.PaymentPeriods,
                    Status = (m.PaymentPeriods != null) ? "<span class=\"blabel warning\">belum bayar</span>" : "<span class=\"blabel success\">lunas</span>"
                };
            return Json(new
            {
                studentData = studentData
            }, JsonRequestBehavior.AllowGet);
        }

        public FileResult GetNotPaid(DateTime pertanggal, string branch)
        {
            //reportData = (from d in db.Invoices// join s in db.Students on d.StudentID equals s.StudentID
            //                 where d.PaymentDate >= datefrom && d.PaymentDate <= dateto //&& s.Branch.ToUpper() == branch.ToUpper()
            //                 select new 
            //    {
            //        DateTimeReport = d.PaymentDate,
            //        StudentCode = d.StudentCode,
            //        StudentName = d.StudentName,
            //        Enrollments = d.Enrollments,
            //        Period = d.PaymentPeriodString,
            //        Price = (d.FixedPrice != null) ? d.FixedPrice : d.Price
            //    }).ToList();
            var studentPayments = db.Database.SqlQuery<StudentPayment>("[dbo].ViewStudentPayment", new object[] { }).ToList();
            var studentData = from m in studentPayments
                              join s in db.Students on m.StudentId equals s.StudentID
                              //where s.Status == "1"
                              where m.PaymentPeriods != null
                              select new
            {
                StudentId = m.StudentId,
                StudentCode = m.StudentCode,
                Name = m.StudentName,
                PaymentPeriods = m.PaymentPeriods,
                Enrollments = string.Join(", ", db.Enrollments.Where(o => o.StudentID == m.StudentId).Select(n => n.Class.ClassCode).ToArray())
            };

            var docx = DocX.Load(Server.MapPath("~/Content/daftarbelumbayar.docx"));
            docx.ReplaceText("[[PERBULAN]]", pertanggal.ToString("MMMM", new CultureInfo("id-ID")).ToUpper());
            docx.ReplaceText("[[KODESISWA]]", (string.Join("\r\n", studentData.Select(s => s.StudentCode).ToArray())));
            docx.ReplaceText("[[NAMASISWA]]", (string.Join("\r\n", studentData.Select(s => s.Name).ToArray())));
            docx.ReplaceText("[[KELASSISWA]]", (string.Join("\r\n", studentData.Select(s => s.Enrollments).ToArray())));
            docx.ReplaceText("[[CABANG]]", branch.ToUpper());
            docx.ReplaceText("[[BULANBAYAR]]", (string.Join("\r\n", studentData.Select(s => s.PaymentPeriods))));
            var memoryStream = new MemoryStream();
            docx.SaveAs(memoryStream);
            return File(memoryStream.ToArray(), "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "daftarbelumbayar.doc");

        }

        [Authorize]
        public ActionResult Pay(int studentId, string fixedprice, string reason, bool isnotpaypenalty, int penalty, string paiddates)
        {
            var listpaiddates = new JavaScriptSerializer().Deserialize<List<DateTime>>(paiddates);
            var students = new List<Payment>();
            students = db.Payments.Where(m => m.StudentID == studentId && m.Paid == false && listpaiddates.Contains(m.PaymentPeriod)).ToList();
            var now = DateTime.Now;
            if (!students.Any())
            {
                students.Add(db.Payments.Where(m => m.StudentID == studentId && m.Paid == true).OrderBy(m => m.PaymentPeriod).ToList().Last());
                students[0].PaymentDate = now;
            }
            else
            {
                foreach (var s in students)
                {
                    s.PaymentDate = now;
                    if (!string.IsNullOrEmpty(fixedprice))
                    {
                        s.FixedPrice = Convert.ToDecimal(fixedprice);
                        s.ReasonFixedPrice = reason;
                    }
                    s.IsNotPayPenalty = isnotpaypenalty;
                    s.Penalty = penalty;
                    s.Paid = true;
                }
            }

            var addedpaiddates = listpaiddates.Where(m => !students.Select(s => s.PaymentPeriod).Contains(m));
            var laststudent = students.Last();
            var addedpayments = new List<Payment>();
            foreach (var a in addedpaiddates)
            {
                addedpayments.Add(new Payment { Enrollments = laststudent.Enrollments, Discount = 0, Penalty = 0, Paid = true, PaymentDate = now, PaymentPeriod = a, Price = laststudent.Price, StudentCode = laststudent.StudentCode, StudentID = laststudent.StudentID, StudentName = laststudent.StudentName });
            }
            var branch = "Pusat";
            if (User.Identity.Name == "mimin" || User.Identity.Name == "linda")
            {
                branch = "Kebayoran";
            }


            if (branch == "Pusat")
            {
                var invoice = new InvoicesPusat
                {
                    Discount = laststudent.Discount,
                    Enrollments = laststudent.Enrollments,
                    FixedPrice = laststudent.FixedPrice,
                    IsNotPayPenalty = laststudent.IsNotPayPenalty,
                    Paid = laststudent.Paid,
                    PaymentDate = laststudent.PaymentDate,
                    PaymentPeriodString = string.Join(", ", listpaiddates.Select(m => m.ToString("MMMM yyyy", new CultureInfo("id-ID"))).ToArray()),
                    Penalty = laststudent.Penalty,
                    Price = students.Sum(m => m.Price) + addedpayments.Sum(p => p.Price),
                    ReasonFixedPrice = laststudent.ReasonFixedPrice,
                    StudentCode = laststudent.StudentCode,
                    StudentID = laststudent.StudentID,
                    StudentName = laststudent.StudentName,
                    AdminUserName = User.Identity.Name,
                    Branch = branch
                };
                db.InvoicesPusat.Add(invoice);
                db.SaveChanges();
                foreach (var s in students)
                {
                    s.InvoiceID = invoice.InvoiceID;
                }
                foreach (var a in addedpayments)
                {
                    a.InvoiceID = invoice.InvoiceID;
                    db.Payments.Add(a);
                }
                db.SaveChanges();
            }
            else
            {
                var invoice = new InvoicesKebayoran
                {
                    Discount = laststudent.Discount,
                    Enrollments = laststudent.Enrollments,
                    FixedPrice = laststudent.FixedPrice,
                    IsNotPayPenalty = laststudent.IsNotPayPenalty,
                    Paid = laststudent.Paid,
                    PaymentDate = laststudent.PaymentDate,
                    PaymentPeriodString = string.Join(", ", listpaiddates.Select(m => m.ToString("MMMM yyyy", new CultureInfo("id-ID"))).ToArray()),
                    Penalty = laststudent.Penalty,
                    Price = students.Sum(m => m.Price) + addedpayments.Sum(p => p.Price),
                    ReasonFixedPrice = laststudent.ReasonFixedPrice,
                    StudentCode = laststudent.StudentCode,
                    StudentID = laststudent.StudentID,
                    StudentName = laststudent.StudentName,
                    AdminUserName = User.Identity.Name,
                    Branch = branch
                };
                db.InvoicesKebayoran.Add(invoice);
                db.SaveChanges();
                foreach (var s in students)
                {
                    s.InvoiceID = invoice.InvoiceID;
                }
                foreach (var a in addedpayments)
                {
                    a.InvoiceID = invoice.InvoiceID;
                    db.Payments.Add(a);
                }
                db.SaveChanges();
            }


            ViewBag.StudentId = studentId;
            return View("PaymentSuccess");
        }

        [Authorize]
        public ActionResult Print(int studentId)
        {
            var student = db.Students.Find(studentId);
            if (student.Branch.ToUpper() == "PUSAT")
            {
                var payment = db.InvoicesPusat.Where(m => m.StudentID == studentId && m.Paid == true && m.PaymentDate.Value.Month == DateTime.Now.Month).ToList();
                if (!payment.Any())
                {
                    ViewBag.Message = "Invoice tidak ditemukan";
                    return View("Index");
                }
                var docx = DocX.Load(Server.MapPath("~/Content/kwitansi.docx"));
                docx.ReplaceText("[[NAMA]]", student.Name);
                docx.ReplaceText("[[NOKWITANSI]]", (payment.Last().InvoiceID.ToString("D5") + "/" + (student.Branch == "Pusat" ? "P" : "K")));
                docx.ReplaceText("[[NOANGGOTA]]", student.StudentCode);
                docx.ReplaceText("[[KELAS]]", string.Join(", ", student.Enrollments.Select(m => m.Class.ClassCode)));
                docx.ReplaceText("[[IURAN]]", payment.Last().PaymentPeriodString);
                docx.ReplaceText("[[JUMLAH]]", "Rp " + (payment.Last().FixedPrice == null ? (payment.Last().Price.Value + payment.Last().Penalty.Value) : payment.Last().FixedPrice.Value).ToString("N2", new CultureInfo("id-ID")));
                docx.ReplaceText("[[TANGGAL]]", DateTime.Now.ToString("dd MMM yyyy", new CultureInfo("id-ID")));
                docx.ReplaceText("[[ADMIN]]", User.Identity.Name);

                var memoryStream = new MemoryStream();
                docx.SaveAs(memoryStream);
                //return File(memoryStream.ToArray(), "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "kwitansi.doc");
                Document document = new Document();
                document.LoadFromStream(memoryStream, FileFormat.Docx);
                //embeds full fonts by default when IsEmbeddedAllFonts is set to true.

                //Save doc file to pdf.
                var memoryStreamPDF = new MemoryStream();
                document.SaveToStream(memoryStreamPDF, FileFormat.PDF);
                return File(memoryStreamPDF.ToArray(), "application/pdf", "kwitansi-" + student.Name + ".pdf");

            }
            else
            {
                var payment = db.InvoicesKebayoran.Where(m => m.StudentID == studentId && m.Paid == true && m.PaymentDate.Value.Month == DateTime.Now.Month).ToList();
                if (!payment.Any())
                {
                    ViewBag.Message = "Invoice tidak ditemukan";
                    return View("Index");
                }
                var docx = DocX.Load(Server.MapPath("~/Content/kwitansi.docx"));
                docx.ReplaceText("[[NAMA]]", student.Name);
                docx.ReplaceText("[[NOKWITANSI]]", (payment.Last().InvoiceID.ToString("D5") + "/" + (student.Branch == "Pusat" ? "P" : "K")));
                docx.ReplaceText("[[NOANGGOTA]]", student.StudentCode);
                docx.ReplaceText("[[KELAS]]", string.Join(", ", student.Enrollments.Select(m => m.Class.ClassCode)));
                docx.ReplaceText("[[IURAN]]", payment.Last().PaymentPeriodString);
                docx.ReplaceText("[[JUMLAH]]", "Rp " + (payment.Last().FixedPrice == null ? (payment.Last().Price.Value + payment.Last().Penalty.Value) : payment.Last().FixedPrice.Value).ToString("N2", new CultureInfo("id-ID")));
                docx.ReplaceText("[[TANGGAL]]", DateTime.Now.ToString("dd MMM yyyy", new CultureInfo("id-ID")));
                docx.ReplaceText("[[ADMIN]]", User.Identity.Name);

                var memoryStream = new MemoryStream();
                docx.SaveAs(memoryStream);
                //return File(memoryStream.ToArray(), "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "kwitansi.doc");
                Document document = new Document();
                document.LoadFromStream(memoryStream, FileFormat.Docx);
                //embeds full fonts by default when IsEmbeddedAllFonts is set to true.

                //Save doc file to pdf.
                var memoryStreamPDF = new MemoryStream();
                document.SaveToStream(memoryStreamPDF, FileFormat.PDF);
                return File(memoryStreamPDF.ToArray(), "application/pdf", "kwitansi-" + student.Name + ".pdf");
            }
            return null;
        }


        public JsonResult GetPaymentDetail(int studentId)
        {
            decimal PENALTYPRICE = 25000;
            var query = new List<Payment>();
            var result = new PaymentDetails();
            query = db.Payments.Where(m => m.StudentID == studentId && m.Paid == false && m.PaymentPeriod <= DateTime.Now).OrderBy(m => m.PaymentPeriod).ToList();
            if (!query.Any())
            {
                //var temp = db.Payments.Where(m => m.StudentID == studentId).OrderBy(m => m.PaymentPeriod).ToList();
                //int i = 0;
                //while (i < temp.Count())
                //{
                //    if (temp[i].Paid != true)
                //    {
                //        query.Add(temp[i - 1]);
                //        break;
                //    }
                //    i++;
                //}
                var query2 = db.Students.Where(m => m.StudentID == studentId).Single();
                var query3 = db.Payments.Where(m => m.StudentID == studentId).OrderBy(m => m.PaymentPeriod).Last();
                result.StudentCode = query2.StudentCode;
                result.StudentId = studentId;
                result.StudentName = query2.Name;
                result.LastClasses = String.Join(",",query2.Enrollments.Select(m=>m.Class.ClassCode));
                result.LastPeriod = query3.PaymentPeriod;
                result.LastPrice = query3.Price;
                result.MonthlyPayments = new List<MonthlyPayment>();
                var monthlyPaymentsString = new StringBuilder();
                monthlyPaymentsString.Append("<table id='tblmonthlypaymentstring'>");
                monthlyPaymentsString.Append("</table>");
                result.MonthlyPaymentsString = monthlyPaymentsString.ToString();
                result.TotalPenalty = result.MonthlyPayments.Sum(m => m.Penalty);
                result.TotalPrice = result.MonthlyPayments.Sum(m => m.Price);
                result.GrandTotalPrice = result.TotalPrice + result.TotalPenalty;
                result.Status = (result.MonthlyPayments != null && result.MonthlyPayments.Count > 0) ? "<span class=\"blabel warning\">belum bayar</span>" : "<span class=\"blabel success\">lunas</span>";

            }
            else
            {
                result.StudentCode = query[0].StudentCode;
                result.StudentId = query[0].StudentID;
                result.StudentName = query[0].StudentName;
                result.MonthlyPayments = new List<MonthlyPayment>();
                var monthlyPaymentsString = new StringBuilder();
                monthlyPaymentsString.Append("<table id='tblmonthlypaymentstring'>");

                foreach (var q in query)
                {
                    //hitung penalty
                    decimal penalty = 0;
                    var now = DateTime.Now;
                    if (q.PaymentPeriod < new DateTime(now.Year, now.Month, 1) || //bulan lalu, atau..
                       (q.PaymentPeriod.Month == now.Month && now.Day > 15)) //lebih dari tanggal 15)
                    {
                        penalty = PENALTYPRICE;
                    }

                    result.MonthlyPayments.Add(new MonthlyPayment
                    {
                        PaymentPeriod = q.PaymentPeriod,
                        Penalty = penalty,
                        Classes = q.Enrollments,
                        Price = q.Price ?? 0
                    });
                    monthlyPaymentsString.AppendFormat("<tr><td><input id='cb-{0}' type='checkbox' checked onchange='cbchange(\"{0}\",\"" + q.PaymentPeriod + "\")'/></td><td>{1}</td><td>{2}</td><td id=\"price-{0}\">{3}</td></tr>", q.PaymentPeriod.ToString("MMM-yyyy"), q.PaymentPeriod.ToString("MMM yyyy"), q.Enrollments, Convert.ToInt32(q.Price));
                }
                monthlyPaymentsString.Append("</table>");
                result.MonthlyPaymentsString = monthlyPaymentsString.ToString();
                result.TotalPenalty = result.MonthlyPayments.Sum(m => m.Penalty);
                result.TotalPrice = result.MonthlyPayments.Sum(m => m.Price);
                result.GrandTotalPrice = result.TotalPrice + result.TotalPenalty;

                result.Status = (result.MonthlyPayments != null && result.MonthlyPayments.Count > 0) ? "<span class=\"blabel warning\">belum bayar</span>" : "<span class=\"blabel success\">lunas</span>";
            }
            return Json(new
        {
            studentData = result
        }, JsonRequestBehavior.AllowGet);
        }

        public PaymentDetails GetPaymentDetail2(int studentId)
        {
            decimal PENALTYPRICE = 25000;
            var query = new List<Payment>();
            var result = new PaymentDetails();
            query = db.Payments.Where(m => m.StudentID == studentId && m.Paid == false && m.PaymentPeriod <= DateTime.Now).OrderBy(m => m.PaymentPeriod).ToList();
            if (!query.Any())
            {
             
                var query2 = db.Students.Where(m => m.StudentID == studentId).Single();
                var query3 = db.Payments.Where(m => m.StudentID == studentId).OrderBy(m => m.PaymentPeriod).Last();
                result.StudentCode = query2.StudentCode;
                result.StudentId = studentId;
                result.StudentName = query2.Name;
                result.LastClasses = String.Join(",", query2.Enrollments.Select(m => m.Class.ClassCode));
                result.LastPeriod = query3.PaymentPeriod;
                result.LastPrice = query3.Price;
                result.MonthlyPayments = new List<MonthlyPayment>();
                var monthlyPaymentsString = new StringBuilder();
                monthlyPaymentsString.Append("<table id='tblmonthlypaymentstring'>");
                monthlyPaymentsString.Append("</table>");
                result.MonthlyPaymentsString = monthlyPaymentsString.ToString();
                result.TotalPenalty = result.MonthlyPayments.Sum(m => m.Penalty);
                result.TotalPrice = result.MonthlyPayments.Sum(m => m.Price);
                result.GrandTotalPrice = result.TotalPrice + result.TotalPenalty;
                result.Status = (result.MonthlyPayments != null && result.MonthlyPayments.Count > 0) ? "<span class=\"blabel warning\">belum bayar</span>" : "<span class=\"blabel success\">lunas</span>";

            }
            else
            {
                result.StudentCode = query[0].StudentCode;
                result.StudentId = query[0].StudentID;
                result.StudentName = query[0].StudentName;
                result.MonthlyPayments = new List<MonthlyPayment>();
                var monthlyPaymentsString = new StringBuilder();
                monthlyPaymentsString.Append("<table id='tblmonthlypaymentstring'>");

                foreach (var q in query)
                {
                    //hitung penalty
                    decimal penalty = 0;
                    var now = DateTime.Now;
                    if (q.PaymentPeriod < new DateTime(now.Year, now.Month, 1) || //bulan lalu, atau..
                       (q.PaymentPeriod.Month == now.Month && now.Day > 15)) //lebih dari tanggal 15)
                    {
                        penalty = PENALTYPRICE;
                    }

                    result.MonthlyPayments.Add(new MonthlyPayment
                    {
                        PaymentPeriod = q.PaymentPeriod,
                        Penalty = penalty,
                        Classes = q.Enrollments,
                        Price = q.Price ?? 0
                    });
                    monthlyPaymentsString.AppendFormat("<tr><td><input id='cb-{0}' type='checkbox' checked onchange='cbchange(\"{0}\",\"" + q.PaymentPeriod + "\")'/></td><td>{1}</td><td>{2}</td><td id=\"price-{0}\">{3}</td></tr>", q.PaymentPeriod.ToString("MMM-yyyy"), q.PaymentPeriod.ToString("MMM yyyy"), q.Enrollments, Convert.ToInt32(q.Price));
                }
                monthlyPaymentsString.Append("</table>");
                result.MonthlyPaymentsString = monthlyPaymentsString.ToString();
                result.TotalPenalty = result.MonthlyPayments.Sum(m => m.Penalty);
                result.TotalPrice = result.MonthlyPayments.Sum(m => m.Price);
                result.GrandTotalPrice = result.TotalPrice + result.TotalPenalty;

                result.Status = (result.MonthlyPayments != null && result.MonthlyPayments.Count > 0) ? "<span class=\"blabel warning\">belum bayar</span>" : "<span class=\"blabel success\">lunas</span>";
            }
            return result;
        }

        public ActionResult OneTimePayment()
        {
            return View();
        }

        [HttpPost]
        public ActionResult OneTimePayment(OneTimePayment model)
        {
            //insert to invoice
            var cl = db.Classes.Find(model.ClassId);
           
            if (cl.Branch.ToUpper() == "PUSAT")
            {
                var invoice = new InvoicesPusat
                {
                    AdminUserName = User.Identity.Name,
                    Branch = cl.Branch,
                    FixedPrice = model.FixedPrice,
                    Paid = true,
                    PaymentDate = DateTime.Now,
                    Price = cl.OneTimePrice,
                    ReasonFixedPrice = model.ReasonFixedPrice,
                    StudentName = model.Name
                };
                db.InvoicesPusat.Add(invoice);

                db.SaveChanges();

                var docx = DocX.Load(Server.MapPath("~/Content/kwitansi.docx"));
                docx.ReplaceText("[[NAMA]]", invoice.StudentName);
                docx.ReplaceText("[[NOKWITANSI]]", (invoice.InvoiceID.ToString("D5") + "/" + (invoice.Branch == "Pusat" ? "P" : "K")));
                docx.ReplaceText("[[NOANGGOTA]]", "[PER KALI DATANG]");
                docx.ReplaceText("[[KELAS]]", cl.ClassCode + " - " + cl.Name);
                docx.ReplaceText("[[IURAN]]", "[PER KALI DATANG]");
                docx.ReplaceText("[[JUMLAH]]", "Rp " + (invoice.FixedPrice == null ? (cl.OneTimePrice.Value) : invoice.FixedPrice.Value).ToString("N2", new CultureInfo("id-ID")));
                docx.ReplaceText("[[TANGGAL]]", DateTime.Now.ToString("dd MMM yyyy", new CultureInfo("id-ID")));
                docx.ReplaceText("[[ADMIN]]", User.Identity.Name);

                var memoryStream = new MemoryStream();
                docx.SaveAs(memoryStream);
                // return File(memoryStream.ToArray(), "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "kwitansi.doc");
                Document document = new Document();
                document.LoadFromStream(memoryStream, FileFormat.Docx);
                //embeds full fonts by default when IsEmbeddedAllFonts is set to true.

                //Save doc file to pdf.
                var memoryStreamPDF = new MemoryStream();
                document.SaveToStream(memoryStreamPDF, FileFormat.PDF);
                return File(memoryStreamPDF.ToArray(), "application/pdf", "kwitansi-" + model.Name + ".pdf");

            }
            else
            {
                var invoice = new InvoicesKebayoran
                {
                    AdminUserName = User.Identity.Name,
                    Branch = cl.Branch,
                    FixedPrice = model.FixedPrice,
                    Paid = true,
                    PaymentDate = DateTime.Now,
                    Price = cl.OneTimePrice,
                    ReasonFixedPrice = model.ReasonFixedPrice,
                    StudentName = model.Name
                };
                db.InvoicesKebayoran.Add(invoice);
                db.SaveChanges();

                var docx = DocX.Load(Server.MapPath("~/Content/kwitansi.docx"));
                docx.ReplaceText("[[NAMA]]", invoice.StudentName);
                docx.ReplaceText("[[NOKWITANSI]]", (invoice.InvoiceID.ToString("D5") + "/" + (invoice.Branch == "Pusat" ? "P" : "K")));
                docx.ReplaceText("[[NOANGGOTA]]", "[PER KALI DATANG]");
                docx.ReplaceText("[[KELAS]]", cl.ClassCode + " - " + cl.Name);
                docx.ReplaceText("[[IURAN]]", "[PER KALI DATANG]");
                docx.ReplaceText("[[JUMLAH]]", "Rp " + (invoice.FixedPrice == null ? (cl.OneTimePrice.Value) : invoice.FixedPrice.Value).ToString("N2", new CultureInfo("id-ID")));
                docx.ReplaceText("[[TANGGAL]]", DateTime.Now.ToString("dd MMM yyyy", new CultureInfo("id-ID")));
                docx.ReplaceText("[[ADMIN]]", User.Identity.Name);

                var memoryStream = new MemoryStream();
                docx.SaveAs(memoryStream);
                var fileResult = File(memoryStream.ToArray(), "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "kwitansi.doc");
                Document document = new Document();
                document.LoadFromStream(memoryStream, FileFormat.Docx);
                //embeds full fonts by default when IsEmbeddedAllFonts is set to true.

                //Save doc file to pdf.
                var memoryStreamPDF = new MemoryStream();
                document.SaveToStream(memoryStreamPDF, FileFormat.PDF);
                return File(memoryStreamPDF.ToArray(), "application/pdf", "kwitansi-" + model.Name + ".pdf");


            }
        }

        public JsonResult GetClassPrice(int classId)
        {
            var cl = db.Classes.Find(classId);
            return Json(cl.OneTimePrice);
        }

        public ActionResult PaymentPage(int StudentId)
        {
            var model = new PaymentDetails();
            model = GetPaymentDetail2(StudentId);
            return View(model);
        }

        [Authorize]
        public bool FillPaymentTable()
        {
            try
            {
                var studentList = db.Students.ToList().Select(m => new
                {
                    StudentId = m.StudentID,
                    StudentCode = m.StudentCode,
                    Name = m.Name,
                    Enrollments = string.Join(", ", m.Enrollments.Select(n => n.Class.ClassCode).ToArray()),
                    TotalPrice = m.Enrollments.Select(n => n.Class.Price).Sum()
                }).ToList();
                var lastPeriod = db.Payments.OrderByDescending(m => m.PaymentPeriod).Select(m => m.PaymentPeriod).First();
                var periodToAdd = lastPeriod.AddMonths(1);
                var listPayment = new List<Payment>();
                while (periodToAdd < DateTime.Now)
                {
                    listPayment = new List<Payment>();
                    foreach (var s in studentList)
                    {
                        listPayment.Add(new Payment
                        {
                            StudentID = s.StudentId,
                            StudentCode = s.StudentCode,
                            StudentName = s.Name,
                            Enrollments = s.Enrollments,
                            Price = s.TotalPrice,
                            Paid = false,
                            PaymentPeriod = periodToAdd
                        });
                    }
                    db.Payments.AddRange(listPayment);
                    db.SaveChanges();
                    periodToAdd = periodToAdd.AddMonths(1);
                }

                
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}
 