﻿using Namarina2.DAL;
using Namarina2.Filters;
using Namarina2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Namarina2.Controllers
{
    //[InitializeSimpleMembership]
    public class WorkHourController : Controller
    {
        private NamarinaDbContext db = new NamarinaDbContext();

        //
        // GET: /WorkHour/
        [Authorize(Roles = "manajer")]
        public ActionResult Index()
        {
            var model = new WorkHour();
            model.Teachers = db.Teachers.ToList();
            return View(model);
        }

    }
}
