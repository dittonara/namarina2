﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Namarina2.Models;
using Namarina2.DAL;
using Namarina2.ViewModels;
using Namarina2.Filters;

namespace Namarina2.Controllers
{
    //[InitializeSimpleMembership]
    public class TeacherController : Controller
    {
        private NamarinaDbContext db = new NamarinaDbContext();

        //
        // GET: /Teacher/
        [Authorize(Roles="manajer")]
        public ActionResult Index()
        {
            return View(db.Teachers.ToList());
        }

        //
        // GET: /Teacher/Details/5
        [Authorize(Roles = "manajer")]
        public JsonResult GetTeachers()
        {
            return Json(new
            {
                teacherData = db.Teachers.Select(m => new
                {
                    TeacherID = m.TeacherID,
                    Name = m.Name,
                    Address = m.Address,
                    MobilePhoneNo = m.MobilePhoneNo
                }).ToList()
            },
                        JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "manajer")]
        public ActionResult Details(int id = 0)
        {
            Teacher teacher = db.Teachers.Find(id);
            if (teacher == null)
            {
                return HttpNotFound();
            }
            return View(teacher);
        }

        //
        // GET: /Teacher/Create
        [Authorize(Roles = "manajer")]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Teacher/Create

        [HttpPost]
        [Authorize(Roles = "manajer")]
        public ActionResult Create(Teacher teacher)
        {
            if (ModelState.IsValid)
            {
                db.Teachers.Add(teacher);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(teacher);
        }

        //
        // GET: /Teacher/Edit/5
        [Authorize(Roles = "manajer")]
        public ActionResult Edit(int id = 0)
        {
            Teacher teacher = db.Teachers.Find(id);
            if (teacher == null)
            {
                return HttpNotFound();
            }
            return View(teacher);
        }

        //
        // POST: /Teacher/Edit/5

        [HttpPost]
        [Authorize(Roles = "manajer")]
        public ActionResult Edit(Teacher teacher)
        {
            if (ModelState.IsValid)
            {
                db.Entry(teacher).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(teacher);
        }

        //
        // GET: /Teacher/Delete/5
        [Authorize(Roles = "manajer")]
        public ActionResult Delete(int id = 0)
        {
            Teacher teacher = db.Teachers.Find(id);
            if (teacher == null)
            {
                return HttpNotFound();
            }
            return View(teacher);
        }

        //
        // POST: /Teacher/Delete/5

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "manajer")]
        public ActionResult DeleteConfirmed(int id)
        {
            Teacher teacher = db.Teachers.Find(id);
            db.Teachers.Remove(teacher);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        [Authorize(Roles = "manajer")]
        public ActionResult AssignClass(int id)
        {
            var teacher = db.Teachers.Find(id);

            var model = new TeacherClass();
            model.TeacherId = id;
            model.TeacherName = teacher.Name;
            model.AssignedClassIds = teacher.TeacherAssignments.Select(m => m.ClassID.Value).ToList();
            model.AssignedClassNames = teacher.TeacherAssignments.Select(m => m.Class.Name).ToList();
            model.BalletClasses = db.Classes.Where(m => m.Type == "Ballet").ToList();
            model.JazzClasses = db.Classes.Where(m => m.Type == "Jazz").ToList();
            model.FitnessClasses = db.Classes.Where(m => m.Type == "Fitness").ToList();
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "manajer")]
        public ActionResult AssignClass(FormCollection form)
        {
            var listSelectedClassId = new List<int>();
            var studentId = Convert.ToInt32(form["StudentId"]);
            if (form["graded"] != null)
            {
                listSelectedClassId.Add(Convert.ToInt32(form["graded"]));
            }
            if (form["vocational"] != null)
            {
                listSelectedClassId.Add(Convert.ToInt32(form["vocational"]));
            }
            if (form["jazzballet"] != null)
            {
                listSelectedClassId.Add(Convert.ToInt32(form["jazzballet"]));
            }
            if (form["jazzhiphop"] != null)
            {
                listSelectedClassId.Add(Convert.ToInt32(form["jazzhiphop"]));
            }
            if (form["paket"] != null)
            {
                listSelectedClassId.Add(Convert.ToInt32(form["paket"]));
            }
            if (form["tambahan"] != null)
            {
                listSelectedClassId.Add(Convert.ToInt32(form["tambahan"]));
            }

            var student = db.Students.Find(studentId);
            foreach (var d in db.Enrollments.Where(m => m.StudentID == studentId))
            {
                db.Enrollments.Remove(d);
            }
            foreach (var c in listSelectedClassId)
            {
                if (c != 0)
                {
                    var enrollment = new Enrollment();
                    enrollment.StudentID = studentId;
                    enrollment.ClassID = c;
                    enrollment.EnrollmentDates = DateTime.Now;

                    student.Enrollments.Add(enrollment);
                }
            }
            db.SaveChanges();

            ViewBag.Message = "Success update class";
            return View("Success");
        }
    }
}