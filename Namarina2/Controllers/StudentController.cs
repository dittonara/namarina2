﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Namarina2.Models;
using Namarina2.DAL;
using Namarina2.ViewModels;
using System.IO;
using System.Globalization;
using Namarina2.Filters;
using Kendo.DynamicLinq;
using Novacode;

namespace Namarina2.Controllers
{
    [InitializeSimpleMembership]
    public class StudentController : Controller
    {
        private NamarinaDbContext db = new NamarinaDbContext();

        //
        // GET: /Student/
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }
        [Authorize]
        [HttpPost]
        public ActionResult GetStudents(string branch, int take, int skip, IEnumerable<Sort> sort, Kendo.DynamicLinq.Filter filter)
        {
            var result = db.Students.Where(o=> o.Branch.ToUpper() == branch.ToUpper()).OrderBy(m=>m.StudentID).Select(m => new
                {
                    StudentId = m.StudentID,
                    StudentCode = m.StudentCode,
                    Name = m.Name,
                    Address = m.Address,
                    MobilePhoneNo = m.MobilePhoneNo,
                    BirthDate = m.BirthDate,
                    Religion = m.Religion,
                    AdmissionDate = m.AdmissionDate,
                    IsBallet = m.IsBallet,
                    IsJazz = m.IsJazz,
                    IsFitness = m.IsFitness,
                    IsNDA = m.IsNDA,
                    Status = (m.Status == "1") ? "Aktif" : "Non-aktif"
                }).ToDataSourceResult(take, skip, sort, filter);

               
            return Json(result);          
        }

        //
        // GET: /Student/Details/5
        [Authorize]
        public ActionResult Details(int id = 0)
        {
            Student student = db.Students.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        //
      

        // GET: /Student/Create
        [Authorize]
        public ActionResult Create(int? id)
        {
            if (id == null || id.Value == 0)
            {
                id = 0;
                var student = new Student();
                return View(student);
            }
            else
            {
                Student student = db.Students.Find(id.Value);

                return View(student);
            }
        }

        //
        // POST: /Student/Create

        private string GetStudentCode(Student student)
        {
            var lastStudent = db.Students.Where(m => m.Name.StartsWith(student.Name.Substring(0, 1))).OrderBy(m => m.StudentCode);
            var branchCode = (student.Branch.ToUpper() == "PUSAT") ? "P" : "K";

            if (lastStudent.Any())
            {
                var lastStudentCode = Convert.ToInt32(lastStudent.ToList().Last().StudentCode.Substring(1, 3));
                return string.Format("{0}{1}/{2}", student.Name.Substring(0, 1).ToUpper(), (lastStudentCode + 1).ToString("D3"), branchCode);
            }
            else
            {
                return string.Format("{0}{1}/{2}", student.Name.Substring(0, 1).ToUpper(), "001", branchCode);
            }
        }

        [HttpPost]
        [Authorize]
        public ActionResult Create(Student student)
        {
            ModelState.Remove("AdmissionDate");
            try
            {
                if (ModelState.IsValid)
                {
                    if (student.StudentID == 0)
                    {
                        student.StudentCode = GetStudentCode(student);
                        try
                        {
                            student.BirthDate = new DateTime(student.Tahun, student.Bulan, student.Tanggal);
                        }
                        catch (Exception) { }
                        db.Students.Add(student);
                        db.SaveChanges();
                        return RedirectToAction("ChangeClass", "Student", new { id = student.StudentID, isRegistration = 1 });
                    }
                    else //existing student
                    {
                        db.Entry(student).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("ChangeClass", "Student", new { id = student.StudentID, isDaftarUlang = 1 });
                    }
                }
            }
            catch (DataException)
            {
                //Log the error (add a variable name after DataException)
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }

            return View(student);
        }

        //
        // GET: /Student/Edit/5
        [Authorize]
        public ActionResult Edit(int id = 0)
        {
            Student student = db.Students.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        //
        // POST: /Student/Edit/5

        [HttpPost]
        [Authorize]
        public ActionResult Edit(Student student)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(student).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException)
            {
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            return View(student);
        }

        //
        // GET: /Student/Delete/5
        [Authorize]
        public ActionResult Delete(int id = 0)
        {
            Student student = db.Students.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        //
        // POST: /Student/Delete/5

        [HttpPost, ActionName("Delete")]
        [Authorize]
        public ActionResult DeleteConfirmed(int id)
        {
            Student student = db.Students.Find(id);
            db.Students.Remove(student);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public FileResult GetBirthday(string branch, int month)
        {
            var birthdaygirl = db.Students.Where(m => m.BirthDate != null && m.Status == "1" && m.BirthDate.Value.Month == month && m.Branch.ToLower() == branch.ToLower()).OrderBy(o => o.BirthDate.Value.Day).ToList();
            
            var docx = DocX.Load(Server.MapPath("~/Content/daftarultah.docx"));            
            docx.ReplaceText("[[BULAN]]", new DateTime(2000,month,1).ToString("MMMM", new CultureInfo("id-ID")).ToUpper());
            docx.ReplaceText("[[DAFTARSISWA]]", (string.Join("\r\n", birthdaygirl.Select(s=> s.Name).ToArray())));
            docx.ReplaceText("[[TANGGALULTAH]]", (string.Join("\r\n", birthdaygirl.Select(s => s.BirthDate.Value.ToString("dd MMMM", new CultureInfo("id-ID"))).ToArray())));
            docx.ReplaceText("[[CABANG]]", branch.ToUpper());
            var memoryStream = new MemoryStream();
            docx.SaveAs(memoryStream);
            return File(memoryStream.ToArray(), "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "ultah.doc");
      
        }

        [Authorize]
        public ActionResult ChangeClass(int id)
        {
            var student = db.Students.Find(id);

            var model = new StudentClass();
            model.StudentId = id;
            model.StudentName = student.Name;
            model.TakenClassIds = student.Enrollments.Select(m => m.ClassID.Value).ToList();
            model.TakenClassNames = student.Enrollments.Select(m => m.Class.Name).ToList();
            model.TakenPackageId = student.ClassPackageID;
            model.BalletClasses = db.Classes.Where(m => m.Type == "Ballet").ToList();
            model.JazzClasses = db.Classes.Where(m => m.Type == "Jazz").ToList();
            model.FitnessClasses = db.Classes.Where(m => m.Type == "Fitness").ToList();
            model.ClassPackages = db.ClassPackages.ToList();
            model.Enrollments = student.Enrollments;
            return View(model);
        }

        public ActionResult RegistrationInvoice(int studentID, int bulanDaftar, int tahunDaftar)
        {
            return View();
        }

        [HttpPost]
        public ActionResult RegistrationInvoice(int StudentID, string fixedprice, string reasonfixedprice, int tahunDaftar, int bulanDaftar)
        {
            bool valid = false;
            if (!string.IsNullOrEmpty(fixedprice) && !string.IsNullOrEmpty(reasonfixedprice))
            {
                valid = true;
            }
            else if (string.IsNullOrEmpty(fixedprice) && string.IsNullOrEmpty(reasonfixedprice))
            {
                valid = true;
            }
            else
            {
                valid = false;
            }
            if (valid)
            {
                var student = db.Students.Find(StudentID);
                student.Status = "1";
                db.SaveChanges();

                var registrationInvoice = new RegistrationInvoice();
                registrationInvoice.PaymentDate = DateTime.Now;
                registrationInvoice.Price = string.IsNullOrEmpty(fixedprice) ? 375000 : Convert.ToDecimal(fixedprice);
                registrationInvoice.ReasonFixedPrice = reasonfixedprice;
                registrationInvoice.StudentID = StudentID;
                registrationInvoice.StudentCode = student.StudentCode;
                registrationInvoice.StudentName = student.Name;
                registrationInvoice.AdminUserName = User.Identity.Name;
                registrationInvoice.PaymentPeriod = new DateTime(tahunDaftar, bulanDaftar, 1);
                
                var invoice = db.RegistrationInvoices.Where(m=>m.StudentID == StudentID && m.PaymentPeriod == registrationInvoice.PaymentPeriod);
                if (invoice.Any())
                {
                    TempData["Error"] = "Murid ini sudah melakukan pendaftaran pada bulan ini";
                    return View("RegistrationInvoice", new { studentId = StudentID });
                }
                else
                {
                    db.RegistrationInvoices.Add(registrationInvoice);
                    db.SaveChanges();
                    var docx = DocX.Load(Server.MapPath("~/Content/kwitansipendaftaran.docx"));
                    docx.ReplaceText("[[NAMA]]", student.Name);
                    docx.ReplaceText("[[NOKWITANSI]]", (registrationInvoice.RegistrationInvoiceID.ToString("D5") + "/" + (student.Branch == "Pusat" ? "P" : "K")));
                    docx.ReplaceText("[[NOANGGOTA]]", student.StudentCode);
                    docx.ReplaceText("[[KELAS]]", string.Join(", ", student.Enrollments.Select(m => m.Class.ClassCode)));
                    docx.ReplaceText("[[JUMLAH]]", "Rp " + registrationInvoice.Price.Value.ToString("N2", new CultureInfo("id-ID")));
                    docx.ReplaceText("[[TANGGAL]]", DateTime.Now.ToString("dd MMM yyyy", new CultureInfo("id-ID")));
                    docx.ReplaceText("[[ADMIN]]", User.Identity.Name);

                    var memoryStream = new MemoryStream();
                    docx.SaveAs(memoryStream);
                    TempData["Success"] = "Pendaftaran berhasil";
                    return File(memoryStream.ToArray(), "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "kwitansipendaftaran.doc");
                }
             }
            else
            {
                TempData["Error"] = "Mohon isi harga pembetulan dan alasannya, atau kosongkan keduanya";
                return View("RegistrationInvoice", new { studentId = StudentID });
            }
        }

        [HttpPost]
        [Authorize]
        public ActionResult ChangeClass(FormCollection form)
        {
            var listSelectedClassId = new List<int>();
            var listSelectedPackageId = new List<int>();
            var studentId = Convert.ToInt32(form["StudentId"]);
            if (form["graded-pusat"] != null)
            {
                listSelectedClassId.Add(Convert.ToInt32(form["graded-pusat"]));
            }
            if (form["vocational-pusat"] != null)
            {
                listSelectedClassId.Add(Convert.ToInt32(form["vocational-pusat"]));
            }
            if (form["special-pusat"] != null)
            {
                listSelectedClassId.Add(Convert.ToInt32(form["special-pusat"]));
            }
            if (form["jazzballet-pusat"] != null)
            {
                listSelectedClassId.Add(Convert.ToInt32(form["jazzballet-pusat"]));
            }
            if (form["jazzhiphop-pusat"] != null)
            {
                listSelectedClassId.Add(Convert.ToInt32(form["jazzhiphop-pusat"]));
            }
            if (form["paket-pusat"] != null)
            {
                if (form["paket-pusat"].ToString().StartsWith("p-"))
                {
                    listSelectedPackageId.Add(Convert.ToInt32(form["paket-pusat"].Substring(2)));
                }
                else
                {
                    listSelectedClassId.Add(Convert.ToInt32(form["paket-pusat"]));
                }
            }
            if (form["tambahan-pusat"] != null)
            {
                listSelectedClassId.Add(Convert.ToInt32(form["tambahan-pusat"]));
            }

            if (form["graded-kebayoran"] != null)
            {
                listSelectedClassId.Add(Convert.ToInt32(form["graded-kebayoran"]));
            }
            if (form["vocational-kebayoran"] != null)
            {
                listSelectedClassId.Add(Convert.ToInt32(form["vocational-kebayoran"]));
            }
            if (form["special-kebayoran"] != null)
            {
                listSelectedClassId.Add(Convert.ToInt32(form["special-kebayoran"]));
            }
            if (form["jazzballet-kebayoran"] != null)
            {
                listSelectedClassId.Add(Convert.ToInt32(form["jazzballet-kebayoran"]));
            }
            if (form["jazzhiphop-kebayoran"] != null)
            {
                listSelectedClassId.Add(Convert.ToInt32(form["jazzhiphop-kebayoran"]));
            }
            if (form["paket-kebayoran"] != null)
            {
                if (form["paket-kebayoran"].ToString().StartsWith("p-"))
                {
                    listSelectedPackageId.Add(Convert.ToInt32(form["paket-kebayoran"].Substring(2)));
                }
                else
                {
                    listSelectedClassId.Add(Convert.ToInt32(form["paket-kebayoran"]));
                }
            }
            if (form["tambahan-kebayoran"] != null)
            {
                listSelectedClassId.Add(Convert.ToInt32(form["tambahan-kebayoran"]));
            }

            if(listSelectedClassId.Count() == 0)
            {
                TempData["Error"] = "Mohon pilih kelas";
                return RedirectToAction("ChangeClass", new { @id = studentId });
            }

            var student = db.Students.Find(studentId);
            foreach (var d in db.Enrollments.Where(m => m.StudentID == studentId))
            {
                db.Enrollments.Remove(d);
            }
            foreach (var c in listSelectedClassId)
            {
                if (c != 0)
                {
                    var enrollment = new Enrollment();
                    enrollment.StudentID = studentId;
                    enrollment.ClassID = c;
                    enrollment.EnrollmentDates = DateTime.Now;

                    student.Enrollments.Add(enrollment);
                }
            }
            foreach (var c in listSelectedPackageId)
            {
                student.ClassPackageID = c;
            }

            db.SaveChanges();
            var tahunDaftar = Convert.ToInt32(form["tahundaftar"]);
            var bulanDaftar = Convert.ToInt32(form["bulandaftar"]);
            var tanggalDaftar = new DateTime();
            try
            {
                tanggalDaftar = new DateTime(tahunDaftar, bulanDaftar, 1);
            }
            catch (Exception) { } 
            if (Convert.ToString(form["isRegistration"]) == "1")
            {
                CalculatePayment(studentId, tanggalDaftar);
                return RedirectToAction("RegistrationInvoice", "Student", new { studentID = studentId, isRegistration = 1, tahunDaftar = tahunDaftar, bulanDaftar = bulanDaftar  });
            }

            if (Convert.ToString(form["isDaftarUlang"]) == "1")
            {
                CalculatePayment(studentId,tanggalDaftar);
                return RedirectToAction("RegistrationInvoice", "Student", new { studentID = studentId, isRegistration = 1, tahunDaftar = tahunDaftar, bulanDaftar = bulanDaftar });
            }

            ViewBag.Message = "Success update class";
            return View("Success");
        }

        public void CalculatePayment(int studentId, DateTime registrationDate)
        {
            //calculate price
            var price = (from c in db.Classes join e in db.Enrollments on c.ClassID equals e.ClassID
                        join s in db.Students on e.StudentID equals s.StudentID
                        where e.StudentID == studentId
                        select c.Price).Sum();
            var student = db.Students.Find(studentId);

            //looping sampai desember

            var enrollment = student.Enrollments;
            var encalasscode = student.Enrollments.Select(m => m.Class.ClassCode);

            //insert payment
            var payment = new Payment
            {
                PaymentDate = null,
                StudentID = studentId,
                Discount = 0,
                Penalty = 0,
                Price = price,
                Paid = false,
                Enrollments = string.Join(", ", student.Enrollments.Select(m => m.Class.ClassCode)),
                PaymentPeriod = registrationDate,
                StudentCode = student.StudentCode,
                StudentName = student.Name
            };            
            
            db.Payments.Add(payment);
            db.SaveChanges();
        }
    }
}