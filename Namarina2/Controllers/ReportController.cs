﻿using Namarina2.DAL;
using Namarina2.Filters;
using Namarina2.Models;
using Novacode;
using Spire.Doc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Namarina2.Controllers
{
    //[InitializeSimpleMembership]
    public class ReportController : Controller
    {
        //
        // GET: /Report/

        private NamarinaDbContext db = new NamarinaDbContext();


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DailyReport()
        {
            return View();
        }
        public ActionResult WeeklyReport()
        {
            return View();
        }
        public ActionResult MonthlyReport()
        {
            return View();
        }

        [Authorize]
        public JsonResult GetReport(DateTime datefrom, DateTime dateto, string branch)
        {
            if (branch == "PUSAT")
            {
                return Json(new
                {
                    reportData = (from d in db.InvoicesPusat// join s in db.Students on d.StudentID equals s.StudentID
                                  where d.PaymentDate >= datefrom && d.PaymentDate <= dateto //&& s.Branch.ToUpper() == branch.ToUpper()
                                  select new
                     {
                         DateTimeReport = d.PaymentDate,
                         StudentCode = d.StudentCode,
                         StudentName = d.StudentName,
                         Enrollments = d.Enrollments,
                         Period = d.PaymentPeriodString,
                         Price = (d.FixedPrice != null) ? d.FixedPrice : d.Price
                     }).ToList()
                },
                            JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    reportData = (from d in db.InvoicesKebayoran// join s in db.Students on d.StudentID equals s.StudentID
                                  where d.PaymentDate >= datefrom && d.PaymentDate <= dateto //&& s.Branch.ToUpper() == branch.ToUpper()
                                  select new
                                  {
                                      DateTimeReport = d.PaymentDate,
                                      StudentCode = d.StudentCode,
                                      StudentName = d.StudentName,
                                      Enrollments = d.Enrollments,
                                      Period = d.PaymentPeriodString,
                                      Price = (d.FixedPrice != null) ? d.FixedPrice : d.Price
                                  }).ToList()
                },
                          JsonRequestBehavior.AllowGet);
            }
        }

        public FileResult GetDailyReport(string branch, DateTime reportdate, string expenses)
        {
            var now = DateTime.Now;
            var admin = User.Identity.Name;
            var listExpenses = new JavaScriptSerializer().Deserialize<List<Expense>>(expenses);
            var reportData = new List<dynamic>();
            if (branch == "Pusat")
            {
                reportData.Add((from d in db.InvoicesPusat// join s in db.Students on d.StudentID equals s.StudentID
                              where d.PaymentDate.Value.Day == reportdate.Day && d.PaymentDate.Value.Month == reportdate.Month && d.PaymentDate.Value.Year == reportdate.Year
                              && d.Branch.ToUpper() == branch.ToUpper()
                              //&& d.AdminUserName == admin
                              select new
                               {
                                   DateTimeReport = d.PaymentDate,
                                   StudentCode = d.StudentCode,
                                   StudentName = d.StudentName,
                                   Enrollments = d.Enrollments,
                                   Period = d.PaymentPeriodString,
                                   Price = (d.FixedPrice != null) ? d.FixedPrice : d.Price,
                                   Penalty = d.Penalty
                               }).ToList());
            }
            else
            {
                reportData.Add((from d in db.InvoicesKebayoran// join s in db.Students on d.StudentID equals s.StudentID
                           where d.PaymentDate.Value.Day == reportdate.Day && d.PaymentDate.Value.Month == reportdate.Month && d.PaymentDate.Value.Year == reportdate.Year
                           && d.Branch.ToUpper() == branch.ToUpper()
                           //&& d.AdminUserName == admin
                           select new
                           {
                               DateTimeReport = d.PaymentDate,
                               StudentCode = d.StudentCode,
                               StudentName = d.StudentName,
                               Enrollments = d.Enrollments,
                               Period = d.PaymentPeriodString,
                               Price = (d.FixedPrice != null) ? d.FixedPrice : d.Price,
                               Penalty = d.Penalty
                           }).ToList());
            }
            var registrationInvoice = from d in db.RegistrationInvoices
                                      where d.PaymentDate.Value.Day == reportdate.Day
                                      && d.PaymentDate.Value.Month == reportdate.Month
                                      && d.PaymentDate.Value.Year == reportdate.Year  
                                      && d.Branch.ToUpper() == branch.ToUpper()
                                      //&& d.AdminUserName == admin
                                      select d;
            decimal totalPenalty = 0;
            decimal totalPayment = 0;
            decimal totalRegistration = 0;

            try
            {
                 totalPenalty = reportData.Sum(m => m.Penalty);
                 totalPayment = reportData.Sum(m => m.Price);

                 totalRegistration = registrationInvoice.Sum(m => m.Price) ?? 0;
            } catch(Exception ex) { }
            var grandTotal = totalPenalty + totalPayment + totalRegistration;
           
            var docx = DocX.Load(Server.MapPath("~/Content/laporanharian.docx"));
            //docx.ReplaceText("[[BULAN]]", new DateTime(2000, month, 1).ToString("MMMM", new CultureInfo("id-ID")).ToUpper());
            docx.ReplaceText("[[TOTALDENDA]]",totalPenalty.ToString("N2", new CultureInfo("id-ID")));
            docx.ReplaceText("[[TOTALPEMBAYARAN]]", totalPayment.ToString("N2", new CultureInfo("id-ID")));
            docx.ReplaceText("[[TOTALPENDAFTARAN]]", totalRegistration.ToString("N2", new CultureInfo("id-ID")));
            docx.ReplaceText("[[GRANDTOTAL]]", grandTotal.ToString("N2", new CultureInfo("id-ID")));
            docx.ReplaceText("[[CABANG]]", branch.ToUpper());
            docx.ReplaceText("[[TANGGAL]]", reportdate.ToString("dd MMM yyyy", new CultureInfo("id-ID")));
            docx.ReplaceText("[[DESKRIPSI]]", (string.Join("\r\n", listExpenses.Select(s => s.name).ToArray())));
            docx.ReplaceText("[[HARGA]]", (string.Join("\r\n", listExpenses.Select(s => "Rp. " + s.price).ToArray())));
            docx.ReplaceText("[[ADMIN]]", User.Identity.Name);
            var memoryStream = new MemoryStream();
            docx.SaveAs(memoryStream);
            // File(memoryStream.ToArray(), "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "laporanharian.doc");

            Document document = new Document();
            document.LoadFromStream(memoryStream, FileFormat.Docx);
            var pdfStream = new MemoryStream();
            document.SaveToStream(pdfStream, FileFormat.PDF);
            return File(pdfStream.ToArray(), "application/pdf", "laporanharian.pdf");

        }

        public FileResult GetWeeklyReport(string branch, DateTime reportdate, string expenses)
        {
            var now = DateTime.Now;
            var admin = User.Identity.Name;
            var listExpenses = new JavaScriptSerializer().Deserialize<List<Expense>>(expenses);
            var reportData = new List<dynamic>();
            if (branch == "Pusat")
            {
                reportData.Add((from d in db.InvoicesPusat// join s in db.Students on d.StudentID equals s.StudentID
                                where d.PaymentDate.Value.Day == reportdate.Day && d.PaymentDate.Value.Month == reportdate.Month && d.PaymentDate.Value.Year == reportdate.Year
                                && d.Branch.ToUpper() == branch.ToUpper()
                                //&& d.AdminUserName == admin
                                select new
                                {
                                    DateTimeReport = d.PaymentDate,
                                    StudentCode = d.StudentCode,
                                    StudentName = d.StudentName,
                                    Enrollments = d.Enrollments,
                                    Period = d.PaymentPeriodString,
                                    Price = (d.FixedPrice != null) ? d.FixedPrice : d.Price,
                                    Penalty = d.Penalty
                                }).ToList());
            }
            else
            {
                reportData.Add((from d in db.InvoicesKebayoran// join s in db.Students on d.StudentID equals s.StudentID
                                where d.PaymentDate.Value.Day == reportdate.Day && d.PaymentDate.Value.Month == reportdate.Month && d.PaymentDate.Value.Year == reportdate.Year
                                && d.Branch.ToUpper() == branch.ToUpper()
                                //&& d.AdminUserName == admin
                                select new
                                {
                                    DateTimeReport = d.PaymentDate,
                                    StudentCode = d.StudentCode,
                                    StudentName = d.StudentName,
                                    Enrollments = d.Enrollments,
                                    Period = d.PaymentPeriodString,
                                    Price = (d.FixedPrice != null) ? d.FixedPrice : d.Price,
                                    Penalty = d.Penalty
                                }).ToList());
            }
            var registrationInvoice = from d in db.RegistrationInvoices
                                      where d.PaymentDate.Value.Day == reportdate.Day
                                      && d.PaymentDate.Value.Month == reportdate.Month
                                      && d.PaymentDate.Value.Year == reportdate.Year
                                      && d.Branch.ToUpper() == branch.ToUpper()
                                      //&& d.AdminUserName == admin
                                      select d;
            decimal totalPenalty = 0;
            decimal totalPayment = 0;
            decimal totalRegistration = 0;

            try
            {
                totalPenalty = reportData.Sum(m => m.Penalty);
                totalPayment = reportData.Sum(m => m.Price);

                totalRegistration = registrationInvoice.Sum(m => m.Price) ?? 0;
            }
            catch (Exception ex) { }
            var grandTotal = totalPenalty + totalPayment + totalRegistration;

            var docx = DocX.Load(Server.MapPath("~/Content/laporanmingguan.docx"));
            //docx.ReplaceText("[[BULAN]]", new DateTime(2000, month, 1).ToString("MMMM", new CultureInfo("id-ID")).ToUpper());
            docx.ReplaceText("[[TOTALDENDA]]", totalPenalty.ToString("N2", new CultureInfo("id-ID")));
            docx.ReplaceText("[[TOTALPEMBAYARAN]]", totalPayment.ToString("N2", new CultureInfo("id-ID")));
            docx.ReplaceText("[[TOTALPENDAFTARAN]]", totalRegistration.ToString("N2", new CultureInfo("id-ID")));
            docx.ReplaceText("[[GRANDTOTAL]]", grandTotal.ToString("N2", new CultureInfo("id-ID")));
            docx.ReplaceText("[[CABANG]]", branch.ToUpper());
            docx.ReplaceText("[[TANGGAL]]", reportdate.ToString("dd MMM yyyy", new CultureInfo("id-ID")));
            docx.ReplaceText("[[DESKRIPSI]]", (string.Join("\r\n", listExpenses.Select(s => s.name).ToArray())));
            docx.ReplaceText("[[HARGA]]", (string.Join("\r\n", listExpenses.Select(s => "Rp. " + s.price).ToArray())));
            docx.ReplaceText("[[ADMIN]]", User.Identity.Name);
            var memoryStream = new MemoryStream();
            docx.SaveAs(memoryStream);
            // return File(memoryStream.ToArray(), "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "laporanmingguan.doc");

            Document document = new Document();
            document.LoadFromStream(memoryStream, FileFormat.Docx);
            var pdfStream = new MemoryStream();
            document.SaveToStream(pdfStream, FileFormat.PDF);
            return File(pdfStream.ToArray(), "application/pdf", "laporanmingguan.pdf");

        }

        public FileResult GetMonthlyReport(string branch, DateTime reportdate, string expenses)
        {
            var now = DateTime.Now;
            var admin = User.Identity.Name;
            var listExpenses = new JavaScriptSerializer().Deserialize<List<Expense>>(expenses);
            var reportData = new List<dynamic>();
            if (branch == "Pusat")
            {
                reportData.Add((from d in db.InvoicesPusat// join s in db.Students on d.StudentID equals s.StudentID
                                where d.PaymentDate.Value.Day == reportdate.Day && d.PaymentDate.Value.Month == reportdate.Month && d.PaymentDate.Value.Year == reportdate.Year
                                && d.Branch.ToUpper() == branch.ToUpper()
                                //&& d.AdminUserName == admin
                                select new
                                {
                                    DateTimeReport = d.PaymentDate,
                                    StudentCode = d.StudentCode,
                                    StudentName = d.StudentName,
                                    Enrollments = d.Enrollments,
                                    Period = d.PaymentPeriodString,
                                    Price = (d.FixedPrice != null) ? d.FixedPrice : d.Price,
                                    Penalty = d.Penalty
                                }).ToList());
            }
            else
            {
                reportData.Add((from d in db.InvoicesKebayoran// join s in db.Students on d.StudentID equals s.StudentID
                                where d.PaymentDate.Value.Day == reportdate.Day && d.PaymentDate.Value.Month == reportdate.Month && d.PaymentDate.Value.Year == reportdate.Year
                                && d.Branch.ToUpper() == branch.ToUpper()
                                //&& d.AdminUserName == admin
                                select new
                                {
                                    DateTimeReport = d.PaymentDate,
                                    StudentCode = d.StudentCode,
                                    StudentName = d.StudentName,
                                    Enrollments = d.Enrollments,
                                    Period = d.PaymentPeriodString,
                                    Price = (d.FixedPrice != null) ? d.FixedPrice : d.Price,
                                    Penalty = d.Penalty
                                }).ToList());
            }
            var registrationInvoice = from d in db.RegistrationInvoices
                                      where d.PaymentDate.Value.Day == reportdate.Day
                                      && d.PaymentDate.Value.Month == reportdate.Month
                                      && d.PaymentDate.Value.Year == reportdate.Year
                                      && d.Branch.ToUpper() == branch.ToUpper()
                                      //&& d.AdminUserName == admin
                                      select d;
            decimal totalPenalty = 0;
            decimal totalPayment = 0;
            decimal totalRegistration = 0;

            try
            {
                totalPenalty = reportData.Sum(m => m.Penalty);
                totalPayment = reportData.Sum(m => m.Price);

                totalRegistration = registrationInvoice.Sum(m => m.Price) ?? 0;
            }
            catch (Exception ex) { }
            var grandTotal = totalPenalty + totalPayment + totalRegistration;

            var docx = DocX.Load(Server.MapPath("~/Content/laporanbulanan.docx"));
            //docx.ReplaceText("[[BULAN]]", new DateTime(2000, month, 1).ToString("MMMM", new CultureInfo("id-ID")).ToUpper());
            docx.ReplaceText("[[TOTALDENDA]]", totalPenalty.ToString("N2", new CultureInfo("id-ID")));
            docx.ReplaceText("[[TOTALPEMBAYARAN]]", totalPayment.ToString("N2", new CultureInfo("id-ID")));
            docx.ReplaceText("[[TOTALPENDAFTARAN]]", totalRegistration.ToString("N2", new CultureInfo("id-ID")));
            docx.ReplaceText("[[GRANDTOTAL]]", grandTotal.ToString("N2", new CultureInfo("id-ID")));
            docx.ReplaceText("[[CABANG]]", branch.ToUpper());
            docx.ReplaceText("[[TANGGAL]]", reportdate.ToString("dd MMM yyyy", new CultureInfo("id-ID")));
            docx.ReplaceText("[[DESKRIPSI]]", (string.Join("\r\n", listExpenses.Select(s => s.name).ToArray())));
            docx.ReplaceText("[[HARGA]]", (string.Join("\r\n", listExpenses.Select(s => "Rp. " + s.price).ToArray())));
            docx.ReplaceText("[[ADMIN]]", User.Identity.Name);
            var memoryStream = new MemoryStream();
            docx.SaveAs(memoryStream);
            // return File(memoryStream.ToArray(), "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "laporanbulanan.doc");

            Document document = new Document();
            document.LoadFromStream(memoryStream, FileFormat.Docx);
            var pdfStream = new MemoryStream();
            document.SaveToStream(pdfStream, FileFormat.PDF);
            return File(pdfStream.ToArray(), "application/pdf", "laporanbulanan.pdf");

        }


    }
}
