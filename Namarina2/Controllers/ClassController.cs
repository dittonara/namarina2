﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Namarina2.Models;
using Namarina2.DAL;
using Namarina2.ViewModels;
using Namarina2.Filters;

namespace Namarina2.Controllers
{
    [InitializeSimpleMembership]
    public class ClassController : Controller
    {
        private NamarinaDbContext db = new NamarinaDbContext();

        //
        // GET: /Class/
        [Authorize]
        public ActionResult Index()
        {
            return View(db.Classes.ToList());
        }

        //
        // GET: /Class/Details/5
        [Authorize]
        public ActionResult Details(int id = 0)
        {
            Class myclass = db.Classes.Find(id);
            var studentInClass = (from e in myclass.Enrollments
                                  join s in db.Students on e.StudentID equals s.StudentID
                                  where e.ClassID == id && s.Status == "1"
                                  select s).ToList();

            if (myclass == null)
            {
                return HttpNotFound();
            }

            if (Request.IsAjaxRequest())
            {
                ViewBag.StudentInClass = studentInClass;
                return PartialView(myclass);
            }

            return View(myclass);
        }
       

        //
        // GET: /Class/Create
        [Authorize(Roles="manajer")]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Class/Create

        [HttpPost]
        [Authorize(Roles = "manajer")]
        public ActionResult Create(Class myclass)
        {
            if (ModelState.IsValid)
            {
                db.Classes.Add(myclass);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(myclass);
        }

        //
        // GET: /Class/Edit/5
        [Authorize(Roles = "manajer")]
        public ActionResult Edit(int id = 0)
        {
            Class myclass = db.Classes.Find(id);
            if (myclass == null)
            {
                return HttpNotFound();
            }
            return View(myclass);
        }

        //
        // POST: /Class/Edit/5

        [HttpPost]
        [Authorize(Roles = "manajer")]
        public ActionResult Edit(Class myclass)
        {
            if (ModelState.IsValid)
            {
                db.Entry(myclass).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(myclass);
        }

        //
        // GET: /Class/Delete/5
        [Authorize(Roles = "manajer")]
        public ActionResult Delete(int id = 0)
        {
            Class myclass = db.Classes.Find(id);
            if (myclass == null)
            {
                return HttpNotFound();
            }
            return View(myclass);
        }

        //
        // POST: /Class/Delete/5

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "manajer")]
        public ActionResult DeleteConfirmed(int id)
        {
            Class myclass = db.Classes.Find(id);
            db.Classes.Remove(myclass);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        [Authorize]
        public JsonResult GetClasses(string branch)
        {
            var result = db.Classes.Where(o=>o.Branch.ToUpper() == branch.ToUpper()).Select(m => new
                {
                    ClassID = m.ClassID,
                    ClassCode = m.ClassCode,
                    Name = m.Name,
                    StudentCount = (from e in m.Enrollments join s in db.Students on e.StudentID equals s.StudentID
                                   where e.ClassID == m.ClassID && s.Status == "1"
                                   select s.StudentID).Count()//m.Enrollments.Where(n=>n.ClassID == m.ClassID).Count()
                }).ToList();
            return Json(new
            {
                classData = result
            },
                        JsonRequestBehavior.AllowGet);
        }
    }
}