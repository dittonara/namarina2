﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Namarina2.Models;
using Namarina2.DAL;
using Namarina2.Filters;

namespace Namarina2.Controllers
{
    [InitializeSimpleMembership]
    public class GuardianController : Controller
    {
        private NamarinaDbContext db = new NamarinaDbContext();

        //
        // GET: /Guardian/

        [Authorize]
        public ActionResult Index(int studentID)
        {
            ViewBag.StudentID = studentID;
            ViewBag.StudentName = db.Students.Find(studentID).Name;
            
            return View();
        }

        [Authorize]
        public JsonResult GetGuardians(int studentId)
        {
            return Json(new
            {
                guardianData = db.Guardians.Where(m=>m.Student.StudentID == studentId).Select(m => new
                {
                    GuardianId = m.GuardianID,
                    Name = m.Name,
                    Relationship = m.Relationship,
                    OfficeAddress = m.OfficeAddress,
                    MobilePhoneNo = m.MobilePhoneNumber,
                    Job = m.Job,
                    Email = m.Email
                }).ToList()
            }, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Guardian/Details/5
        [Authorize]
        public ActionResult Details(int id = 0)
        {
            Guardian guardian = db.Guardians.Find(id);
            if (guardian == null)
            {
                return HttpNotFound();
            }
            return View(guardian);
        }

        //
        // GET: /Guardian/Create
        [Authorize]
        public ActionResult Create(int studentID)
        {
            ViewBag.StudentID = studentID;
            return View();
        }

        //
        // POST: /Guardian/Create

        [HttpPost]
        [Authorize]
        public ActionResult Create(Guardian guardian)
        {
            if (ModelState.IsValid)
            {
                db.Guardians.Add(guardian);
                db.SaveChanges();
                return RedirectToAction("Index", new { @studentId = guardian.StudentID });
            }

            return View(guardian);
        }

        //
        // GET: /Guardian/Edit/5
        [Authorize]
        public ActionResult Edit(int id = 0)
        {
            Guardian guardian = db.Guardians.Find(id);
            if (guardian == null)
            {
                return HttpNotFound();
            }
            return View(guardian);
        }

        //
        // POST: /Guardian/Edit/5

        [HttpPost]
        [Authorize]
        public ActionResult Edit(Guardian guardian)
        {
            if (ModelState.IsValid)
            {
                db.Entry(guardian).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { @studentId = guardian.StudentID });
            }
            return View(guardian);
        }

        //
        // GET: /Guardian/Delete/5
        [Authorize]
        public ActionResult Delete(int id = 0)
        {
            Guardian guardian = db.Guardians.Find(id);
            if (guardian == null)
            {
                return HttpNotFound();
            }
            return View(guardian);
        }

        //
        // POST: /Guardian/Delete/5

        [HttpPost, ActionName("Delete")]
        [Authorize]
        public ActionResult DeleteConfirmed(int id)
        {
            Guardian guardian = db.Guardians.Find(id);
            db.Guardians.Remove(guardian);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}